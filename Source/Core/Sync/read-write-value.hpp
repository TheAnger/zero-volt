/*
 * read-write-value.hpp
 *
 *  Created on: 29 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_SYNC_READ_WRITE_VALUE_HPP_
#define SOURCE_CORE_SYNC_READ_WRITE_VALUE_HPP_

#include "read-write-lock.hpp"

namespace sync {

  template< typename Type > class read_write_value : read_write_lock
  {
      friend class value_token;

      Type instance;

    public:

      template< char prev, char mode > class value_token : token< prev, mode >
      {
          friend class read_write_value;

          read_write_value * rwv = nullptr;

          value_token () = delete;

          value_token (const value_token &) = delete;

          inline value_token (read_write_value * v)
            : token< prev, mode >( v ), rwv( v ) {
          }

        public:

          inline value_token (value_token && t)
            : token< prev, mode >( std::move( t ) ), rwv( t.rwv ) {
            t.rwv = nullptr;
          }

          inline auto nolock () const -> value_token< mode, 'U' > {
            return {rwv};
          }

          inline auto reader () const -> value_token< mode, 'R' > {
            return {rwv};
          }

          inline auto writer () const -> value_token< mode, 'W' > {
            return {rwv};
          }

          inline auto operator * () const -> Type & {
            return rwv->instance;
          }

          inline auto operator -> () const -> Type * {
            return &rwv->instance;
          }
      };

      inline auto nolock () -> value_token< 'U', 'U' > {
        return {this};
      }

      inline auto reader () -> value_token< 'U', 'R' > {
        return {this};
      }

      inline auto writer () -> value_token< 'U', 'W' > {
        return {this};
      }
  };

}

#endif /* SOURCE_CORE_SYNC_READ_WRITE_VALUE_HPP_ */
