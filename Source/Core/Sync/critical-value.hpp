/*
 * critical-value.hpp
 *
 *  Created on: 27 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_SYNC_CRITICAL_VALUE_HPP_
#define SOURCE_CORE_SYNC_CRITICAL_VALUE_HPP_

#include "critical-section.hpp"

namespace sync {

  template< typename Type > class critical_value : critical_section
  {
      friend class token;

      Type instance;

    public:

      template< bool locked > class token : section< locked >
      {
          friend class critical_value;

          critical_value * cv = nullptr;

          token () = delete;

          token (const token &) = delete;

          inline token (critical_value * c)
            : cv( c ), section< locked >( c ) {
          }

        public:

          inline token (token && t)
            : section< locked >( std::move( t ) ), cv( t.cv ) {
            t.cv = nullptr;
          }

          inline auto turn () const -> token< not locked > {
            return {cv};
          }

          inline auto operator * () const -> Type & {
            return cv->instance;
          }

          inline auto operator -> () const -> Type * {
            return &cv->instance;
          }
      };

      inline auto guard () -> token< true > {
        return {this};
      }
  };

}

#endif /* SOURCE_CORE_SYNC_CRITICAL_VALUE_HPP_ */
