/*
 * critical-section.hpp
 *
 *  Created on: 27 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_SYNC_CRITICAL_SECTION_HPP_
#define SOURCE_CORE_SYNC_CRITICAL_SECTION_HPP_

#include "sync-base.hpp"

namespace sync {

  class critical_section
  {
      friend class section;

      CRITICAL_SECTION cs;

    public:

      critical_section (critical_section &&) = delete;

      critical_section (const critical_section &) = delete;

      inline critical_section () {
        InitializeCriticalSection( &cs );
      }

      inline ~critical_section () {
        DeleteCriticalSection( &cs );
      }

      inline void enter () {
        EnterCriticalSection( &cs );
      }

      inline void leave () {
        LeaveCriticalSection( &cs );
      }

    private:

      template< bool locked > inline void begin_lock () {
        enter();
      }

      template< bool locked > inline void end_lock () {
        leave();
      }

    public:

      template< bool locked = true > class section
      {
          friend class critical_section;

          critical_section * cs = nullptr;

          section () = default;

          section (const section &) = delete;

        protected:

          inline section (critical_section * c)
            : cs( c ) {
            if ( cs )
              cs->begin_lock< locked >();
          }

        public:

          inline section (section && s)
            : cs( s.cs ) {
            s.cs = nullptr;
          }

          inline ~section () {
            if ( cs )
              cs->end_lock< locked >();
          }

          inline auto turn () -> section< not locked > {
            return {cs};
          }
      };

      inline auto guard () -> section< true > {
        return {this};
      }

  };

  template< > inline void critical_section::begin_lock< false > () {
    leave();
  }
  template< > inline void critical_section::end_lock< false > () {
    enter();
  }

}

#endif /* SOURCE_CORE_SYNC_CRITICAL_SECTION_HPP_ */
