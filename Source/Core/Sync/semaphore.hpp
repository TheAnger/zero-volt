/*
 * semaphore.hpp
 *
 *  Created on: 27 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_SYNC_SEMAPHORE_HPP_
#define SOURCE_CORE_SYNC_SEMAPHORE_HPP_

#include "sync-base.hpp"

namespace sync {

  class semaphore
  {
    public:

      class primitive
      {
          HANDLE sem;

        public:

          inline primitive (unsigned initial = 0) {
            sem = CreateSemaphoreA( nullptr, initial, MAXLONG, nullptr );
          }

          inline ~primitive () {
            CloseHandle( sem );
          }

          inline void post (unsigned count = 1) {
            ReleaseSemaphore( sem, count, nullptr );
          }

          inline void wait () {
            WaitForSingleObject( sem, INFINITE );
          }

          inline auto try_wait () -> bool {
            return not WaitForSingleObject( sem, 0UL );
          }
      };

    private:

      primitive sem;

      std::atomic< int > count;

    public:

      inline auto try_wait () -> bool {
        for ( int value = count.load( std::memory_order_relaxed ); value > 0; )
          if ( count.compare_exchange_strong( value, value - 1, std::memory_order_acquire ) )
            return true;

        return false;
      }

      inline void wait () {
        if ( not try_wait() )
          if ( 0 > --count )
            sem.wait();
      }

      inline void post () {
        if ( 1 > ++count )
          sem.post();
      }

      inline void delta (int chng) {
        if ( not chng )
          return;

        if ( chng > 0 )
          while ( chng-- )
            post();

        else
          while ( chng++ )
            wait();
      }

      inline auto get () const -> int {
        return count;
      }

      inline semaphore (unsigned val = 0)
        : sem( val ), count( val ) {
      }
  };

}

#endif /* SOURCE_CORE_SYNC_SEMAPHORE_HPP_ */
