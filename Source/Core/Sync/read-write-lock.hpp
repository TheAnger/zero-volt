/*
 * read-write-lock.hpp
 *
 *  Created on: 29 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_SYNC_READ_WRITE_LOCK_HPP_
#define SOURCE_CORE_SYNC_READ_WRITE_LOCK_HPP_

#include "critical-section.hpp"

#include "semaphore.hpp"

namespace sync {

  class read_write_lock : critical_section, semaphore::primitive
  {
      friend class token;

      std::atomic< unsigned > readers { 0 };

    public:

      read_write_lock (read_write_lock &&) = delete;

      read_write_lock (const read_write_lock &) = delete;

      inline read_write_lock ()
        : primitive( 1 ) {
      }

      inline void writelock () {
        wait();
      }

      inline void writeunlock () {
        post();
      }

      inline void readlock () {
        auto lock = guard();
        if ( 1 == ++readers )
          writelock();
      }

      inline void readunlock () {
        auto lock = guard();
        if ( 0 == --readers )
          writeunlock();
      }

    private:

      template< char mode = 'U' > inline void begin_lock () {
      }

      template< char mode = 'U' > inline void end_lock () {
      }

    public:

      template< char prev = 'U', char mode = 'W' > class token
      {
          friend class read_write_lock;

          read_write_lock * rwl = nullptr;

          token () = default;

          token (const token &) = delete;

        protected:

          inline token (read_write_lock * l)
            : rwl( l ) {
            if ( rwl )
              rwl->end_lock< prev >(), rwl->begin_lock< mode >();
          }

        public:

          inline token (token && t)
            : rwl( t.rwl ) {
            t.rwl = nullptr;
          }

          inline ~token () {
            if ( rwl )
              rwl->end_lock< mode >(), rwl->begin_lock< prev >();
          }

          inline auto nolock () -> token< mode, 'U' > {
            return {rwl};
          }

          inline auto reader () -> token< mode, 'R' > {
            return {rwl};
          }

          inline auto writer () -> token< mode, 'W' > {
            return {rwl};
          }
      };

      inline auto nolock () -> token< 'U', 'U' > {
        return {this};
      }

      inline auto reader () -> token< 'U', 'R' > {
        return {this};
      }

      inline auto writer () -> token< 'U', 'W' > {
        return {this};
      }
  };

  template< > inline void read_write_lock::begin_lock< 'W' > () {
    writelock();
  }
  template< > inline void read_write_lock::end_lock< 'W' > () {
    writeunlock();
  }
  template< > inline void read_write_lock::begin_lock< 'R' > () {
    readlock();
  }
  template< > inline void read_write_lock::end_lock< 'R' > () {
    readunlock();
  }

}

#endif /* SOURCE_CORE_SYNC_READ_WRITE_LOCK_HPP_ */
