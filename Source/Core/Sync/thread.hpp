/*
 * thread.hpp
 *
 *  Created on: 28 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_SYNC_THREAD_HPP_
#define SOURCE_CORE_SYNC_THREAD_HPP_

#include "critical-section.hpp"

#include <process.h>

namespace sync {

  class thread : critical_section
  {
    public:

      using func = std::function< void (thread*) >;

      using id_t = uint32_t;

    private:

      enum class run_state
      {
        inert, starting, running, terminating
      };

      func main;
      id_t threadid { 0 };
      run_state state { run_state::inert };
      HANDLE handle { nullptr };

      inline void caller () {
        guard(), state = run_state::running;
        if ( main )
          main( this );
        guard(), state = run_state::inert;
      }

      __attribute__((__force_align_arg_pointer__)) static unsigned __stdcall proxy (void * data) {
        return static_cast< thread * >( data )->caller(), _endthreadex( 0 ), 0;
      }

      inline auto createthread () -> bool {
        decltype(_beginthreadex( 0, 0, 0, 0, 0, 0 )) thandle;
        id_t tid;
        if ( not (thandle = _beginthreadex( nullptr, 0, &thread::proxy, this, 0, &tid )) )
          return false;
        handle = reinterpret_cast< HANDLE >( thandle );
        threadid = tid;
        return true;
      }

      inline void closethread () {
        if ( not handle )
          return;
        CloseHandle( handle );
        handle = nullptr;
      }

    public:

      static inline auto id () -> id_t {
        return GetCurrentThreadId();
      }

      inline auto get_id () -> id_t {
        return guard(), threadid;
      }

      inline auto running () -> bool {
        return guard(), state > run_state::inert;
      }

      inline auto killed () -> bool {
        return guard(), state == run_state::inert or state == run_state::terminating;
      }

      inline auto start () -> bool {
        auto lock = guard();
        if ( state != run_state::inert )
          return false;
        closethread();
        state = run_state::starting;
        if ( createthread() )
          return true;
        state = run_state::inert;
        return false;
      }

      inline void wait_for_exit () {
        decltype(handle) h;
        if ( guard(), h = handle )
          WaitForSingleObject( h, INFINITE );
      }

      inline void signal_exit () {
        auto lock = guard();
        if ( handle and state == run_state::running )
          state = run_state::terminating;
      }

      inline void terminate () {
        signal_exit(), wait_for_exit();
      }

      thread () = delete;
      thread (thread &&) = delete;
      thread (const thread &) = delete;

      inline thread (func f)
        : main( f ) {
      }

      inline ~thread () {
        terminate();
        guard(), closethread();
      }
  };

}

#endif /* SOURCE_CORE_SYNC_THREAD_HPP_ */
