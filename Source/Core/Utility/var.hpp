/*
 * var.hpp
 *
 *  Created on: 1 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_UTILITY_VAR_HPP_
#define SOURCE_CORE_UTILITY_VAR_HPP_

#include "../general.hpp"

struct var
{
    enum class type
    {
      empty, boolean, number, pointer, string, map
    };

    using string = std::string;
    using map = std::map< var, var >;

    var ();
    ~var ();
    var (var &&);
    var (const var &);

    var (bool);
    var (double);
    var (void *);
    var (string *);
    var (map *);

    void reset ();
    void copy (const var &);

    auto getBool () const -> bool;
    auto getNumber () const -> double;
    auto getPointer () const -> void *;
    auto getString () const -> string *;
    auto getMap () const -> map *;
    auto getType () const -> type;

    auto operator = (var &&) -> var &;
    auto operator = (const var &) -> var &;

    auto operator [] (const var &) -> var &;
    auto operator [] (const var &) const -> const var &;

    auto operator == (const var &) const -> bool;
    auto operator != (const var &) const -> bool;

    auto operator < (const var &) const -> bool;
    auto operator > (const var &) const -> bool;
    auto operator <= (const var &) const -> bool;
    auto operator >= (const var &) const -> bool;

    class map_navigator
    {
        const var::map * m = nullptr;

        void copy (const map_navigator &);

      public:

        map_navigator () = default;
        map_navigator (map_navigator &&);
        map_navigator (const map_navigator &);

        map_navigator (const map *);

        operator const map* () const;

        auto readprop (const var &, var &, map_navigator &) -> bool; // true => map returned

        auto operator= (map_navigator &&) -> map_navigator &;
        auto operator= (const map_navigator &) -> map_navigator &;
        auto operator= (const map *) -> map_navigator &;
    };

  private:

    union
    {
        bool ubool = false;
        double unum;
        void * uany;
        std::shared_ptr< void > uptr;
    };

    type vtype = type::empty;

    template< var::type > struct deleter_info
    {
    };

    template< var::type T > struct deleter
    {
        void operator() (void * ptr) {
          delete static_cast< typename deleter_info< T >::ctype >( ptr );
        }
    };

    template< var::type T > void new_uptr_with_delete (void *);
};

template< > struct var::deleter_info< var::type::string >
{
    using ctype = var::string *;
};

template< > struct var::deleter_info< var::type::map >
{
    using ctype = var::map *;
};

#endif /* SOURCE_CORE_UTILITY_VAR_HPP_ */
