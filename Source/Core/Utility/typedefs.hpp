/*
 * typedefs.hpp
 *
 *  Created on: 27 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_UTILITY_TYPEDEFS_HPP_
#define SOURCE_CORE_UTILITY_TYPEDEFS_HPP_

#include "../general.hpp"
#include "var.hpp"

namespace util {

  typedef std::set< id > set_id;

  typedef std::queue< id > queue_id;

  typedef std::map< id, id > map_id;
  typedef std::map< id, bool > map_bool;

  typedef std::map< id, var::map > map_vmap;

}

#endif /* SOURCE_CORE_UTILITY_TYPEDEFS_HPP_ */
