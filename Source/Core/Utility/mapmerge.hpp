/*
 * mapmerge.hpp
 *
 *  Created on: 27 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_UTILITY_MAPMERGE_HPP_
#define SOURCE_CORE_UTILITY_MAPMERGE_HPP_

#include "../general.hpp"
#include "typedefs.hpp"

namespace util {

  /*
   * merge two maps using an optional merge function to decide equal-key situations
   */

  template< typename M >
  inline void mm_op (typename M::iterator & Dest, typename M::const_iterator & Src) {
    // overwrite key's value in dest by default, no special treatment of map values
    Dest->second = Src->second;
  }

  template< typename M >
  inline void mapmerge (M & dest, const M & src) {

    auto DestIt = dest.begin();
    auto SrcIt = src.begin();

    while ( SrcIt != src.end() and DestIt != dest.end() )

      // src's value comes before dest's
      if ( SrcIt->first < DestIt->first )
        dest.insert( DestIt, *SrcIt++ );

      // dests's value comes before src's
      else if ( SrcIt->first != DestIt->first )
        ++DestIt;

      // merge operation
      else
        mm_op< M >( DestIt, SrcIt ), ++DestIt, ++SrcIt;

    dest.insert( SrcIt, src.end() );
  }

  template< >
  inline void mm_op< var::map > (var::map::iterator & Dest, var::map::const_iterator & Src) {

    // treat scalars per normal case when either values are not maps themselves
    if ( Dest->second.getType() != var::type::map or Src->second.getType() != var::type::map )
      Dest->second = Src->second;

    // recurse into map-valued values, and repeat from the top
    else
      mapmerge( *Dest->second.getMap(), *Src->second.getMap() );
  }

  template< >
  inline void mm_op< map_vmap > (map_vmap::iterator & Dest, map_vmap::const_iterator & Src) {

    // plain old recursion, uses var::map specialization
    mapmerge( Dest->second, Src->second );
  }

}

#endif /* SOURCE_CORE_UTILITY_MAPMERGE_HPP_ */
