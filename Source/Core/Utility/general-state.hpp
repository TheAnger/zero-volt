/*
 * general-state.hpp
 *
 *  Created on: 27 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_UTILITY_GENERAL_STATE_HPP_
#define SOURCE_CORE_UTILITY_GENERAL_STATE_HPP_

#include "../general.hpp"

namespace util {

  template< class EndClass, class BaseData >
  struct general_state : public BaseData
  {
      /*
       * must be implemented:
       * void EndClass::merge( EndClass::partial & other );
       */

      struct partial : public BaseData
      {
//          template< class, class > friend struct general_state;
          friend struct general_state< EndClass, BaseData > ;

          EndClass & RootState;

          inline ~partial () {
            RootState.merge( *this );
          }

        private:

          partial () = delete;

          inline partial (EndClass & rs)
            : RootState( rs ) {
          }
      };

      inline auto partial () -> partial {
        return {static_cast< EndClass& >( *this )};
      }
  };

}
#endif /* SOURCE_CORE_UTILITY_GENERAL_STATE_HPP_ */
