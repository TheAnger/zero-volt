/*
 * var.cpp
 *
 *  Created on: 2 Aug 2016
 *      Author: Anger
 */

#include "var.hpp"

// var common constructors / destructors

var::var () {
  // no-op required for discriminated union, cannot 'default' this constructor
}

var::~var () {
  reset();
}

var::var (var && v) {
  copy( v );
}

var::var (const var & v) {
  copy( v );
}

// var data constructors

template< var::type T > void var::new_uptr_with_delete (void * p) {
  new ( &uptr ) decltype(uptr)( p, deleter< T >() ), vtype = T;
}

var::var (bool b)
  : ubool( b ), vtype( type::boolean ) {
}

var::var (double n)
  : unum( n ), vtype( type::number ) {
}

var::var (void * a)
  : uany( a ), vtype( type::pointer ) {
}

var::var (string * s) {
  new_uptr_with_delete< type::string >( s );
}

var::var (map * m) {
  new_uptr_with_delete< type::map >( m );
}

// var state control methods

void var::reset () {

  if ( type::pointer < vtype )
    uptr.~shared_ptr();

  vtype = type::empty;
  ubool = false;
}

void var::copy (const var & v) {

  switch ( reset(), (vtype = v.vtype) ) {

    case type::number:
      unum = v.unum;
      return;

    case type::pointer:
      uany = v.uany;
      return;

    case type::string:
    case type::map:
      new ( &uptr ) decltype(uptr)( v.uptr );
      return;

    default:
      ubool = v.ubool;
  }
}

// var data / info / get functions

auto var::getBool () const -> bool {
  return ubool;
}

auto var::getNumber () const -> double {
  return unum;
}

auto var::getPointer () const -> void * {
  return uany;
}

auto var::getString () const -> string * {
  return static_cast< string* >( uptr.get() );
}

auto var::getMap () const -> map * {
  return static_cast< map* >( uptr.get() );
}

auto var::getType () const -> type {
  return vtype;
}

// var assignment operators

auto var::operator = (var && v) -> var & {
  return copy( v ), *this;
}

auto var::operator = (const var & v) -> var & {
  return copy( v ), *this;
}

// var map access operators

auto var::operator [] (const var & k) -> var & {
  return getMap()->operator[]( k );
}

auto var::operator [] (const var & k) const -> const var & {
  return getMap()->operator[]( k );
}

// var equality operators

auto var::operator == (const var & that) const -> bool {

  if ( this == &that )
    return true;

  if ( vtype != that.vtype )
    return false;

  switch ( vtype ) {

    case type::empty:
      return true;

    case type::boolean:
      return ubool == that.ubool;

    case type::number:
      return unum == that.unum;

    case type::pointer:
      return uany == that.uany;

    case type::string: {
      auto x = getString(), y = that.getString();
      return x and y ? *x == *y : x == y;
    }

    case type::map: {
      auto x = getMap(), y = that.getMap();
      return x and y ? *x == *y : x == y;
    }

    default:
      return false;
  }
}

auto var::operator != (const var & that) const -> bool {
  return not operator==( that );
}

// var comparison operators

auto var::operator < (const var & that) const -> bool {

  if ( this == &that )
    return true;

  if ( vtype != that.vtype )
    return vtype < that.vtype;

  switch ( vtype ) {

    case type::empty:
      return false;

    case type::boolean:
      return not ubool and that.ubool;

    case type::number:
      return unum < that.unum;

    case type::pointer:
      return uany < that.uany;

    case type::string: {
      auto x = getString(), y = that.getString();
      return x and y ? *x < *y : x < y;
    }

    case type::map: {
      auto x = getMap(), y = that.getMap();
      return x and y ? *x < *y : x < y;
    }

    default:
      return false;
  }
}

auto var::operator > (const var & that) const -> bool {
  return that.operator<( *this );
}

auto var::operator <= (const var & that) const -> bool {
  return not that.operator<( *this );
}

auto var::operator >= (const var & that) const -> bool {
  return not operator<( that );
}

// var map navigator

void var::map_navigator::copy (const map_navigator & other) {
  m = other.m;
}

var::map_navigator::map_navigator (map_navigator && other) {
  copy( other );
}
var::map_navigator::map_navigator (const map_navigator & other) {
  copy( other );
}

var::map_navigator::map_navigator (const var::map * other)
  : m( other ) {
}

var::map_navigator::operator const var::map* (void) const {
  return m;
}

auto var::map_navigator::readprop (const var & name,
                                  var & scalar_result,
                                  map_navigator & map_result) -> bool {
  // true => map returned

  if ( not m )
    return scalar_result.reset(), false;

  auto prop = m->find( name );

  if ( prop == m->end() )
    return scalar_result.reset(), false;

  if ( prop->second.getType() != var::type::map )
    return scalar_result = prop->second, false;

  return map_result.m = prop->second.getMap(), true;
}

auto var::map_navigator::operator= (const map_navigator & other) -> map_navigator & {
  return copy( other ), *this;
}
auto var::map_navigator::operator= (map_navigator && other) -> map_navigator & {
  return copy( other ), *this;
}
auto var::map_navigator::operator= (const map * other) -> map_navigator & {
  return m = other, *this;
}
