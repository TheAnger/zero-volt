/*
 * sync.hpp
 *
 *  Created on: 27 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_SYNC_HPP_
#define SOURCE_CORE_SYNC_HPP_

#include "general.hpp"

#include "Sync/sync-base.hpp"
#include "Sync/semaphore.hpp"
#include "Sync/critical-section.hpp"
#include "Sync/critical-value.hpp"
#include "Sync/read-write-lock.hpp"
#include "Sync/read-write-value.hpp"
#include "Sync/thread.hpp"

#endif /* SOURCE_CORE_SYNC_HPP_ */
