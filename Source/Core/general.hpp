/*
 * general.hpp
 *
 *  Created on: 1 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_GENERAL_HPP_
#define SOURCE_CORE_GENERAL_HPP_

#include <iostream>
#include <cstdlib>
#include <memory>
#include <new>

#include <functional>
#include <atomic>
#include <string>
#include <vector>
#include <array>
#include <stack>
#include <queue>
#include <list>
#include <set>
#include <map>

#include <windows.h> // has to be done, unfortunately

namespace util {

  using id = uint32_t;

  /*
   * can clear anything via swap, STL or otherwise - std::queue, looking at you
   */
  template< typename Container >
  void clear (Container & c) {
    Container t;
    std::swap( t, c );
  }

  /*
   * write-free map lookup with default value
   */
  template< typename M, typename K, typename V >
  inline V mapfind (const M & map, const K & key, const V val) {
    auto I = map.find( key );
    return map.end() == I ? val : I->second;
  }

}

#endif /* SOURCE_CORE_GENERAL_HPP_ */
