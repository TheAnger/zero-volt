/*
 * wrapper.hpp
 *
 *  Created on: 27 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_LUA_WRAPPER_HPP_
#define SOURCE_CORE_LUA_WRAPPER_HPP_

#include "lua-base.hpp"

class lua
{
  public:

    using rawptr = lua_State *;

  private:

    rawptr state = nullptr;

    std::stack< int > topstack;

  public:

    lua (rawptr ref = nullptr);
    ~lua ();

    // avoid passing *this
    lua (const lua &) = delete;
    lua (lua &&) = delete;

    operator rawptr () const;

    void release_state ();
    void close_state ();
    auto open_state () -> bool;
    auto reset () -> bool;
    auto valid () -> bool;

    // call with x args and expect y results
    auto call (int narg = 0, int nres = 0) -> bool;

    auto callfile (const char *) -> bool;
    auto loadfile (const char *) -> bool;
    auto geterror () -> const char *;

    auto gettop () const -> int;
    void settop (int) const;
    void pushtop ();
    void poptop ();

    auto push (const var &) -> bool;
    auto peek (int, var &) -> bool;
};

#endif /* SOURCE_CORE_LUA_WRAPPER_HPP_ */
