/*
 * lua-base.hpp
 *
 *  Created on: 27 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_LUA_LUA_BASE_HPP_
#define SOURCE_CORE_LUA_LUA_BASE_HPP_

#include "../utility.hpp"

#include <lua.hpp>

#define _nidx "__newindex"
#define _idx "__index"

#endif /* SOURCE_CORE_LUA_LUA_BASE_HPP_ */
