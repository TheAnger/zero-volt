/*
 * wrapper.cpp
 *
 *  Created on: 27 Aug 2016
 *      Author: Anger
 */

#include "wrapper.hpp"

lua::lua (rawptr ref)
  : state( ref ), topstack() {
  open_state();
}

lua::~lua () {
  close_state();
}

////////////////

lua::operator lua::rawptr () const {
  return state;
}

////////////////

void lua::release_state () {
  state = nullptr;
}

void lua::close_state () {

  if ( not state )
    return;

  lua_close( state );

  release_state();
}

auto lua::open_state () -> bool {

  if ( state )
    return true;

  if ( not (state = luaL_newstate()) )
    return false;

  return luaL_openlibs( state ), true;
}

auto lua::reset () -> bool {
  return close_state(), open_state();
}

auto lua::valid () -> bool {
  return bool( state );
}

////////////////

auto lua::call (int narg, int nres) -> bool {
  //todo pass stack trace lua function's absolute index, oh, and build one while at it
  return not lua_pcallk( state, narg, nres, 0, 0, nullptr );
}

////////////////

auto lua::loadfile (const char * filename) -> bool {
  return state and not luaL_loadfile( state, filename );
}

auto lua::callfile (const char * filename) -> bool {

  if ( not loadfile( filename ) )
    return false;

  if ( call() )
    return true;

  return lua_pop( state, 1 ), false;
}

auto lua::geterror () -> const char * {
  return lua_tolstring( state, -1, nullptr );
}

////////////////

auto lua::gettop () const -> int {
  return lua_gettop( state );
}

void lua::settop (int top) const {
  lua_settop( state, top );
}

void lua::pushtop () {
  topstack.push( lua_gettop( state ) );
}

void lua::poptop () {
  if ( not topstack.empty() )
    lua_settop( state, topstack.top() ), topstack.pop();
}

////////////////

auto lua_rgs (lua::rawptr & stack, unsigned & depth, unsigned & max_depth) -> bool {

  if ( depth <= max_depth )
    return true;

  max_depth = depth;

  return lua_checkstack( stack, 3 );
}

auto lua_push (lua::rawptr stack, const var & V, unsigned & max_depth, unsigned depth = 1) -> bool {

  if ( not lua_rgs( stack, depth, max_depth ) )
    return false;

  switch ( V.getType() ) {

    case var::type::empty:
      return lua_pushnil( stack ), true;

    case var::type::boolean:
      return lua_pushboolean( stack, V.getBool() ), true;

    case var::type::number:
      return lua_pushnumber( stack, V.getNumber() ), true;

    case var::type::pointer:
      return lua_pushlightuserdata( stack, V.getPointer() ), true;

    case var::type::string:
      if ( auto s = V.getString() )
        return lua_pushlstring( stack, s->data(), s->size() ), true;
      else
        return false;

    case var::type::map:
      if ( auto m = V.getMap() ) {

        const unsigned depth_1 = depth + 1;
        const int reset = lua_gettop( stack );
        const int table = reset + 1;

        if ( m->empty() )
          return lua_createtable( stack, 4, 4 ), true;

        const int s = m->size() >> 1 | 4;
        lua_createtable( stack, s, s );

        for ( const auto & kv : *m )

          if ( lua_push( stack, kv.first, max_depth, depth_1 )
            and lua_push( stack, kv.second, max_depth, depth_1 ) )
            lua_rawset( stack, table );

          else
            return lua_settop( stack, reset ), false;

        return true;

      } else
        return false;

    default:
      return false;
  }
}

auto lua_peek (lua::rawptr stack,
               int pos,
               var & V,
               unsigned & max_depth,
               unsigned depth = 1) -> bool {

  if ( not lua_rgs( stack, depth, max_depth ) )
    return false;

  switch ( lua_type( stack, pos ) ) {

    case LUA_TNONE:
    case LUA_TNIL:
      return V.reset(), true;

    case LUA_TBOOLEAN:
      return V.operator =( (bool) lua_toboolean( stack, pos ) ), true;

    case LUA_TNUMBER:
      return V.operator =( (double) lua_tonumberx( stack, pos, nullptr ) ), true;

    case LUA_TLIGHTUSERDATA:
    case LUA_TUSERDATA:
      return V.operator =( lua_touserdata( stack, pos ) ), true;

    case LUA_TSTRING: {
      size_t l;

      if ( const char * s = lua_tolstring( stack, pos, &l ) )
        if ( auto v = new var::string( s, l ) )
          return V.operator =( v ), true;

      return V.reset(), false;
    }

    case LUA_TTABLE:
      if ( auto m = new var::map ) {

        V.operator =( m );

        const unsigned depth_1 = depth + 1;
        const int t = lua_gettop( stack );
        const int key = t + 1, val = t + 2;

        lua_pushnil( stack );

        for ( var k, v; lua_settop( stack, key ), lua_next( stack, pos ); )

          if ( lua_peek( stack, key, k, max_depth, depth_1 )
            and lua_peek( stack, val, v, max_depth, depth_1 ) )
            m->insert( { k, v } );

          else if ( lua_type( stack, key ) == LUA_TFUNCTION
            or lua_type( stack, val ) == LUA_TFUNCTION )
            continue;

          else
            return V.reset(), lua_settop( stack, t ), false;

        lua_settop( stack, t );

        return true;

      } else
        return V.reset(), false;

    default:
      return V.reset(), false;
  }
}

auto lua::push (const var & V) -> bool {
  unsigned max_depth = 0;
  return valid() and lua_push( state, V, max_depth );
}

auto lua::peek (int pos, var & V) -> bool {
  unsigned max_depth = 0;
  return valid() and lua_peek( state, lua_absindex( state, pos ), V, max_depth );
}

