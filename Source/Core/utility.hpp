/*
 * utility.hpp
 *
 *  Created on: 1 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_UTILITY_HPP_
#define SOURCE_CORE_UTILITY_HPP_

#include "general.hpp"

#include "Utility/var.hpp"
#include "Utility/typedefs.hpp"
#include "Utility/mapmerge.hpp"
#include "Utility/general-state.hpp"


#endif /* SOURCE_CORE_UTILITY_HPP_ */
