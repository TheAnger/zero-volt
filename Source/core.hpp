/*
 * core.hpp
 *
 *  Created on: 1 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_CORE_HPP_
#define SOURCE_CORE_HPP_

#include "Core/general.hpp"

#include "Core/utility.hpp"
#include "Core/sync.hpp"
#include "Core/lua.hpp"

#endif /* SOURCE_CORE_HPP_ */
