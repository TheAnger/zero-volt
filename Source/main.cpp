/*
 * main.cpp
 *
 *  Created on: 2 Aug 2016
 *      Author: Anger
 */

#include "zero-volt.hpp"

auto main () -> int {
  return zero_volt::main(), 0;
}

