/*
 * engine.hpp
 *
 *  Created on: 29 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_ENGINE_HPP_
#define SOURCE_ENGINE_HPP_

#include "core.hpp"

class engine
{
    friend class module;

    engine (engine &&) = delete;
    engine (const engine &) = delete;

  protected:

    engine () = default;

    enum class subsystem
    {
      scripting, graphics, input, resource, audio, physics, network
    };

  public:

    class module
    {
        friend class engine;

      protected:

        module (engine *, const subsystem);

        engine * const Engine;

      public:

        static constexpr module * const nullmod = nullptr;

        const subsystem subtype;

        std::atomic< bool > loaded { false }, initialized { false };

        virtual ~module ();

        virtual auto init () -> bool;
        virtual void uninit ();
    };

  protected:

    sync::read_write_value< std::map< subsystem, module* > > modules;

    template< typename Mod > auto add_module () -> bool;

    auto initialize_modules () -> bool;

    void uninitialize_modules ();

    void flush_modules ();

    template< subsystem > auto module_ready () -> bool;
};

#endif /* SOURCE_ENGINE_HPP_ */
