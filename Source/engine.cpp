/*
 * engine.cpp
 *
 *  Created on: 29 Aug 2016
 *      Author: Anger
 */

#include "engine.hpp"

engine::module::module (engine * e, const subsystem s)
  : Engine( e ), subtype( s ) {

  if ( not Engine )
    return;

  {
    auto modules = Engine->modules.reader();

    if ( auto mod = util::mapfind( *modules, subtype, nullmod ) )
      if ( mod->loaded )
        return;
  }

  {
    auto modules = Engine->modules.writer();

    (*modules)[ subtype ] = this;
    loaded = true;
  }
}

engine::module::~module () {

  if ( not Engine )
    return;

  auto modules = Engine->modules.reader();

  loaded = false;

  (*modules)[ subtype ] = nullmod;
}

auto engine::module::init () -> bool {
  // default module cannot be initialized
  return false;
}

void engine::module::uninit () {
  // default module has nothing to uninitialize
  return;
}

template< typename mod > auto engine::add_module () -> bool {

  static_assert( std::is_base_of< module, mod >::value , "module derivative expected" );

  return true;
}

auto engine::initialize_modules () -> bool {

  auto mods = modules.reader();

  if ( not mods->size() )
    return false;

  std::queue< module* > init_queue;

  for ( auto & imod : *mods )
    if ( auto & mod = imod.second )
      if ( mod->loaded and not mod->initialized )
        init_queue.push( mod );

  for ( int remaining, failures = 0; (remaining = init_queue.size()); init_queue.pop() ) {
    auto & mod = init_queue.front();

    if ( mod->init() )
      failures = 0; // successful load, maybe everything else will work now?

    else if ( remaining <= ++failures )
      return false; // any more and we'd be going in circles

    else
      init_queue.push( mod ); // retry loading this module
  }

  return true;
}

void engine::uninitialize_modules () {

  auto mods = modules.reader();

  for ( auto & imod : *mods )
    if ( auto mod = imod.second )
      if ( mod->loaded and mod->initialized )
        delete mod;
}

template< engine::subsystem ss > auto engine::module_ready () -> bool {

  auto mods = modules.reader();

  if ( auto mod = util::mapfind( *mods, ss, module::nullmod ) )
    if ( mod->loaded and mod->initialized )
      return true;
}
