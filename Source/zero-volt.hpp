/*
 * zero-volt.hpp
 *
 *  Created on: 29 Aug 2016
 *      Author: Anger
 */

#ifndef SOURCE_ZERO_VOLT_HPP_
#define SOURCE_ZERO_VOLT_HPP_

#include "engine.hpp"

class zero_volt : protected engine
{
  public:

    static void main ();
};

#endif /* SOURCE_ZERO_VOLT_HPP_ */
