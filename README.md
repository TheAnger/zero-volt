Zero Volt
=========
0V :: effortless game programming

_creator & owner_ **The Anger** `::` genesis (dot) erode (at) googlemail (you know what I mean; bots & scanners do not)

About
-----
A scalable game engine and game design framework. My own design from my own research and experimenting over a decade with different architectures.

Some features
-------------
More a wish-list than an actual feature list. However, these things are possible by design and are on the way.

*   Old Tech - older computers expected to function well, while performance on newer computers can skyrocket thanks to data-parallelization allowing limitless scalability
*   Integrates with Bullet3D Physics Library as an engine module
*   Powered by SFML - able to utilize graphics, audio, gamepad & other inputs, and internet / multi-play
*   Scripting powered by Lua 5.2 with abstractions to allow parallel stacks to process the game state
*   Simple callback interface for multi-stage object processing and data caching
*   Modular engine design - new subsystems and game state modification api's can be potentially hot-loaded

License
-------
Part of the point of making this is so people can learn to make games, and make them well. The same goes for a genuinely good idea, why not share it around. But if you're making a profit, then - and let's be fair here - if I've helped you make that profit it's only right that I at least be acknowledged for _that_.

Subject to these conditions - rather reasonable ones - you are permitted to use and modify the source for free:

1.  That you do not call your projects using this code or derivative works, the same thing as this project - not limited to but including the names "0V", "Zero Volt", "ZVOS", "ZVAPI".
2.  You cannot and will not claim ownership over the _original code_ - in the form it is originally available and/or provided, authenticity verifiable by the owner against known current and/or prior versions.
3.  This README.md is supplied as is, with the name containing "README" and one of the names applicable to #1.
4.  That you refer to this project by any of the names applicable to #1 as "the original source of the code", and attribute it to the author of this project, in any project using the original code or a derivative work.
5.  Derivative works cannot be claimed to be owned by the owner of this project; as such any original improvements or alterations are the property of the owner of those alterations et al, but not the underlying concepts still relied upon - those invented by the owner of this project, or by extension works this project fundamentally relies upon, itself.
6.  For a derivative work to be considered distinct from original code, it must: A. be conceptually different from the original code - permutations achieving the same effective concepts and/or processes with no meaningful differences from the original code in these respects, cannot reasonably be considered a derivative work; and B. be using a significant portion of the original code or a derivative thereof - _approximately_ %25 or more would be _reasonably_ considered to be "using a significant portion".
7.  That you do not profit from the use of the _original code_. If you are to profit from a derivative work, the "0V" logo must be _reasonably displayed_ - displayed for long enough and prominently enough for it to be recognized, in at least one of: an intro screen, a credits screen, a product logo, a recurring texture / image (need not be prominent).
8.  This file and its contents is considered part of the original code it came with. The owner may change this file in subsequent original code.
9.  Where in doubt, please consult the owner to clarify permissive free use.

In the event the conditions are unsuitable, but use of the original code (whether as is, as a permutation or as a derivative work) is still desired, please contact the owner to discuss a potential arrangement.
